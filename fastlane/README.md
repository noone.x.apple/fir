fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios copyP8

```sh
[bundle exec] fastlane ios copyP8
```

Description of what the lane does

### ios createApp

```sh
[bundle exec] fastlane ios createApp
```



### ios privatePrepare

```sh
[bundle exec] fastlane ios privatePrepare
```



### ios privateRelease

```sh
[bundle exec] fastlane ios privateRelease
```



### ios f2d

```sh
[bundle exec] fastlane ios f2d
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
